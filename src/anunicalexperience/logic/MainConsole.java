package anunicalexperience.logic;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import anunicalexperience.logic.AI.DifficultyConstant;
import anunicalexperience.logic.AI.PlayerAIConnection;
import anunicalexperience.logic.manager.GameManagerMultiplayer;
import anunicalexperience.logic.manager.PlayerAction;
import anunicalexperience.logic.network.MultiPlayer;
import anunicalexperience.logic.objects.Direction;

public class MainConsole {

	public static Direction toDirection(String direction) {
		if (direction.equals("a")) {
			return Direction.LEFT;
		} else if (direction.equals("d")) {
			return Direction.RIGHT;
		}
		return Direction.NULL;
	}

	public static void main(String[] args) throws Exception {
		System.out.println(args[0]);
		MultiPlayer multiPlayer = new MultiPlayer("127.0.0.1", 2727, args[0]);
		new PlayerAIConnection("127.0.0.1", 2727, DifficultyConstant.RANDOM);
		GameManagerMultiplayer myGameManager = multiPlayer.getMyGameManager();
		GameManagerMultiplayer enemyGameManager = multiPlayer
				.getEnemyGameManager();
		BufferedReader buffereReader = new BufferedReader(
				new InputStreamReader(System.in));
		myGameManager.start();
		enemyGameManager.start();
		while (!enemyGameManager.isGameOver()) {
			if (!myGameManager.isGameOver()) {
				System.out.println("MY WORLD..............................");
				myGameManager.drawMatrix();
				System.out.println("END OF MY WORLD.......................");
				System.out.println();
				System.out.println("insert direction");
				String buffer = buffereReader.readLine();
				Direction direction = MainConsole.toDirection(buffer);
				myGameManager.getPlayer().setDirection(direction);
				System.out.println();
				System.out.println();
				myGameManager.update();
			}
			PlayerAction playerAction = enemyGameManager.update();
			System.out.println("ENEMY WORDL..........");
			if (playerAction.getShoot()) {
				System.out.println("BOOM");
			}
			System.out.println("books="
					+ enemyGameManager.getPlayer().getNumberOfBooks());
			enemyGameManager.drawMatrix();
			System.out.println("END OF WNWMY WORLD.....................");
			System.out.println();
		}
		System.out.println("Game Over " + multiPlayer.getLooser());
	}
	// public static void main(String[] args) throws IOException {
	// GameManagerSinglePlayer gameManager = new GameManagerSinglePlayer();
	// BufferedReader buffereReader = new BufferedReader(
	// new InputStreamReader(System.in));
	//
	// gameManager.start();
	// while (!gameManager.isGameOver()) {
	// gameManager.drawMatrix();
	//
	// System.out.println("insert direction");
	// String buffer = buffereReader.readLine();
	// Direction direction = MainConsole.toDirection(buffer);
	// gameManager.getPlayer().setDirection(direction);
	// gameManager.update();
	// System.out.println();
	// System.out.println();
	// try {
	// Thread.sleep(50);
	// } catch (InterruptedException e) {
	// e.printStackTrace();
	// }
	// }
	// System.out.println("GAME OVER");
	// }
}
