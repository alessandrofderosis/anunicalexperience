package anunicalexperience.logic.world;

import java.io.IOException;

import anunicalexperience.logic.network.RowHandler;
import anunicalexperience.logic.objects.Empty;
import anunicalexperience.logic.objects.LoaderWeapons;
import anunicalexperience.logic.objects.ObjectWithPosition;
import anunicalexperience.logic.objects.Potion;
import anunicalexperience.logic.objects.Vehicle;
import anunicalexperience.logic.objects.character.Professor;
import anunicalexperience.logic.objects.character.Student;

public class NetworkWorld extends WorldImpl {

	private final RowHandler rowHandler;
	private int numberOfUpdate = 1;

	public NetworkWorld(RowHandler rowHandler) throws IOException {
		this.rowHandler = rowHandler;
		super.initMatrix(getInitialMatrix());
	}

	private ObjectWithPosition[][] getInitialMatrix() throws IOException {
		final ObjectWithPosition[][] defaultWorld = new ObjectWithPosition[WorldImpl.HEIGHT][WorldImpl.WIDTH];
		for (int i = 0; i < 10; i++) {
			defaultWorld[i] = getNewRow();
		}
		return defaultWorld;
	}

	private ObjectWithPosition[] copyRow(ObjectWithPosition[] row) {
		ObjectWithPosition[] copiedRow = new ObjectWithPosition[WorldImpl.WIDTH];
		for (int i = 0; i < copiedRow.length; i++) {
			ObjectWithPosition objectWithPosition = row[i];
			if (objectWithPosition instanceof Empty) {
				copiedRow[i] = new Empty(objectWithPosition.getX(),
						objectWithPosition.getY());
			} else if (objectWithPosition instanceof Student) {
				copiedRow[i] = new Student(objectWithPosition.getX(),
						objectWithPosition.getY());
			} else if (objectWithPosition instanceof Professor) {
				copiedRow[i] = new Professor(objectWithPosition.getX(),
						objectWithPosition.getY());
			} else if (objectWithPosition instanceof Vehicle) {
				copiedRow[i] = new Vehicle(objectWithPosition.getX(),
						objectWithPosition.getY());
			} else if (objectWithPosition instanceof Potion) {
				copiedRow[i] = new Potion(objectWithPosition.getX(),
						objectWithPosition.getY());
			} else if (objectWithPosition instanceof LoaderWeapons) {
				copiedRow[i] = new LoaderWeapons(objectWithPosition.getX(),
						objectWithPosition.getY());
			}
		}
		return copiedRow;
	}

	@Override
	protected ObjectWithPosition[] getNewRow() {
		return copyRow(this.rowHandler.getNewRow(this.numberOfUpdate++));
	}
}
