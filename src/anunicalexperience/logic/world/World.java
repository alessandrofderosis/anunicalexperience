package anunicalexperience.logic.world;

import anunicalexperience.logic.objects.ObjectWithPosition;

public interface World {

	public abstract ObjectWithPosition getCell(int x, int y);

	/**
			 */
	public abstract int getWidth();

	/**
				 */
	public abstract int getHeight();

	/**
					 */
	public abstract void update();

	/**
						 */
	public abstract void reset();

}
