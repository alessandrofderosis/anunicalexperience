package anunicalexperience.logic.world;

public class WrongMatrix extends RuntimeException {

	public WrongMatrix(String message) {
		super(message);
	}

}
