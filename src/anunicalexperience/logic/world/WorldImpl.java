package anunicalexperience.logic.world;

import java.util.ArrayList;

import anunicalexperience.logic.objects.Empty;
import anunicalexperience.logic.objects.LoaderWeapons;
import anunicalexperience.logic.objects.MoveableUpDown;
import anunicalexperience.logic.objects.ObjectWithPosition;
import anunicalexperience.logic.objects.Obstacle;
import anunicalexperience.logic.objects.Potion;
import anunicalexperience.logic.objects.Vehicle;
import anunicalexperience.logic.objects.character.Professor;
import anunicalexperience.logic.objects.character.Student;

public class WorldImpl implements World {

	private static final int DISTANCE_OF_FULL_ROW = 200;
	private int numberOfFullRows = 2;
	public final static int WIDTH = 3;
	public final static int HEIGHT = 10;

	/**
	 * @uml.property name="worldMatrix"
	 */
	private ObjectWithPosition[][] defaultMatrix = null;
	private ObjectWithPosition[][] matrix = null;
	private long createdMetres = 50;

	protected WorldImpl() {
	}

	public WorldImpl(ObjectWithPosition[][] matrix) {
		initMatrix(matrix);
	}

	protected void initMatrix(ObjectWithPosition[][] matrix) {
		this.defaultMatrix = new ObjectWithPosition[WorldImpl.HEIGHT][WorldImpl.WIDTH];
		if (checkMatrix(matrix)) {
			for (int i = 0; i < matrix.length; i++) {
				for (int j = 0; j < matrix[i].length; j++) {
					this.defaultMatrix[i][j] = matrix[i][j];
				}
			}
			this.matrix = matrix;
		}
	}

	private Boolean checkMatrix(ObjectWithPosition[][] matrix) {
		if (matrix.length != WorldImpl.HEIGHT
				|| matrix[0].length != WorldImpl.WIDTH) {
			throw new WrongMatrix("Excepted ObjectWithPosition matrix ["
					+ WorldImpl.HEIGHT + "] [" + WorldImpl.WIDTH
					+ "]. \nProvided matrix[" + matrix.length + "] ["
					+ matrix[0].length + "] !");
		}

		for (int i = matrix.length - 2; i >= 0; i--) {
			ObjectWithPosition[] currentRow = matrix[i];
			ObjectWithPosition[] previousRow = matrix[i + 1];

			if (!checkRows(previousRow, currentRow)) {
				throw new WrongMatrix("Unplayable MatrixWorld");
			}
		}
		return true;
	}

	private Boolean checkRows(ObjectWithPosition[] previousRow,
			ObjectWithPosition[] currentRow) {
		for (int j = 0; j < WorldImpl.WIDTH; j++) {
			if (currentRow[j] instanceof Empty
					&& previousRow[j] instanceof Empty) {
				return true;
			}
		}
		return false;
	}

	@Override
	public ObjectWithPosition getCell(int x, int y) {
		// System.out.println(this.matrix[y][x].getX());
		return this.matrix[y][x] == null ? new Empty(x, y) : this.matrix[y][x];
	}

	@Override
	public int getWidth() {
		return WorldImpl.WIDTH;
	}

	@Override
	public int getHeight() {
		return WorldImpl.HEIGHT;
	}

	@Override
	public void update() {
		for (int i = this.matrix.length - 2; i >= 0; i--) {
			for (int j = 0; j < this.matrix[i].length; j++) {
				if (this.matrix[i][j] instanceof MoveableUpDown) {
					MoveableUpDown moveableUpDown = (MoveableUpDown) this.matrix[i][j];
					moveableUpDown.setY(i + 1);
					this.matrix[i + 1][j] = moveableUpDown;

				}
			}
		}

		this.matrix[0] = getNewRow();
		this.createdMetres += 50;
	}

	protected ObjectWithPosition[] getNewRow() {
		ObjectWithPosition[] newRow = new ObjectWithPosition[WorldImpl.WIDTH];
		ObjectWithPosition[] oldRow = this.matrix[0];

		for (int i = 0; i < newRow.length; i++) {
			newRow[i] = new Empty(i, 0);
		}

		if (this.createdMetres % DISTANCE_OF_FULL_ROW == 0) {
			this.numberOfFullRows += 50;
			int busyTiles = 0;

			ArrayList<Integer> freePositionsOfNewRow = new ArrayList<Integer>(
					WorldImpl.WIDTH);

			for (int i = 0; i < oldRow.length; i++) {
				if (oldRow[i] instanceof Obstacle) {
					busyTiles++;
				}

				// Init free positions
				freePositionsOfNewRow.add(i);
			}

			int numberOfObstaclesToInsert = 0;
			if (busyTiles == 0) {
				// +1 means: one obstacle at least;
				numberOfObstaclesToInsert = (int) (Math.random()
						* WorldImpl.WIDTH - 1) + 1;
			} else {
				numberOfObstaclesToInsert = (int) (Math.random() * WorldImpl.WIDTH);
			}

			// adding obstacles to new row
			if (numberOfObstaclesToInsert > 0) {

				if (busyTiles < WorldImpl.WIDTH - 1) {
					do {
						int index = (int) (Math.random() * freePositionsOfNewRow
								.size());
						// 0 because it's always the first row
						newRow[index] = getRandomObstacle(index, 0);
						freePositionsOfNewRow.remove(index);
						--numberOfObstaclesToInsert;
					} while (busyTiles == 0 && numberOfObstaclesToInsert > 0);
				}

				for (int i = 0; i < oldRow.length
						&& numberOfObstaclesToInsert > 0; i++) {
					if (oldRow[i] instanceof Obstacle
							&& !(newRow[i] instanceof Obstacle)) {
						newRow[i] = getRandomObstacle(i, 0);
						--numberOfObstaclesToInsert;
						// freePositionsOfNewRow.remove(i);
					}
				}
			}

			if (this.numberOfFullRows % LoaderWeapons.CHECKPOINT == 0) {
				for (int i = 0; i < freePositionsOfNewRow.size(); i++) {
					newRow[freePositionsOfNewRow.get(i)] = new LoaderWeapons(
							freePositionsOfNewRow.get(i), 0);
				}
			}

			long module = this.numberOfFullRows % Potion.CHECKPOINT;
			if (module == 0) {
				for (int i = 0; i < freePositionsOfNewRow.size(); i++) {
					newRow[freePositionsOfNewRow.get(i)] = new Potion(
							freePositionsOfNewRow.get(i), 0);
				}
			}
		}
		return newRow;
	}

	private Obstacle getRandomObstacle(int x, int y) {
		int random = (int) (Math.random() * 3);
		switch (random) {
		case 0:
			return new Student(x, y);
		case 1:
			return new Professor(x, y);
		default:
			return new Vehicle(x, y);
		}
	}

	@Override
	public void reset() {
		for (int i = 0; i < this.matrix.length; i++) {
			for (int j = 0; j < this.matrix[i].length; j++) {
				this.matrix[i][j] = this.defaultMatrix[i][j];
			}
		}
	}

	/**
	 * Getter of the property <tt>Matrix</tt>
	 * 
	 * @return Returns the matrix.
	 * @uml.property name="worldMatrix"
	 */
	public ObjectWithPosition[][] getWorldMatrix() {
		return this.matrix;
	}

	/**
	 * Setter of the property <tt>Matrix</tt>
	 * 
	 * @param Matrix
	 *            The matrix to set.
	 * @uml.property name="worldMatrix"
	 */
	public void setWorldMatrix(ObjectWithPosition[][] worldMatrix) {
		this.matrix = worldMatrix;
	}
}
