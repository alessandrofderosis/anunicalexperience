package anunicalexperience.logic.manager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import anunicalexperience.logic.AI.VirtualPlayerAI;
import anunicalexperience.logic.network.PlayerActionsHandler;
import anunicalexperience.logic.network.RowHandler;
import anunicalexperience.logic.objects.Empty;
import anunicalexperience.logic.objects.character.Player;
import anunicalexperience.logic.world.NetworkWorld;
import anunicalexperience.logic.world.WorldImpl;
import anunicalexperience.logic.world.WrongMatrix;

public class GameManagerMultiplayer extends AbstractGameManager {

	public interface UpdaterListener {
		void update(GameManagerMultiplayer gameManager);
	}

	private final List<UpdaterListener> updaterListeners = new ArrayList<GameManagerMultiplayer.UpdaterListener>();

	private final RowHandler rowHandler;
	private final PlayerActionsHandler actionsPlayerHandler;
	private Boolean aShootingHappened;

	public GameManagerMultiplayer(RowHandler rowHandler,
			PlayerActionsHandler playerActionsHandler) {
		this.rowHandler = rowHandler;
		this.actionsPlayerHandler = playerActionsHandler;
	}

	/**
	 * @throws IOException
	 */
	@Override
	public void start() {
		try {
			this.world = new NetworkWorld(this.rowHandler);
		} catch (IOException e) {
			e.printStackTrace();
			this.player = null;
		}
		this.player = new Player(this.world, WorldImpl.WIDTH / 2,
				WorldImpl.HEIGHT - 1);

		if (this.player.getX() != WorldImpl.WIDTH / 2
				|| this.player.getY() != WorldImpl.HEIGHT - 1) {
			throw new RuntimeException(
					"Player must be in the middle of the last row of the world!");
		}

		if (!(this.world.getCell(this.player.getX(), this.player.getY()) instanceof Empty)) {
			throw new WrongMatrix("Position of player must be empty");
		}
	}

	@Override
	public PlayerAction update() {
		this.world.update();
		if (this.actionsPlayerHandler != null) {
			PlayerAction action = this.actionsPlayerHandler.getAction();
			if (action.getShoot()) {
				this.player.shoot();
			} else {
				this.player.setDirection(action.getDirection());
			}
		}
		this.aShootingHappened = this.player.getShooting();
		if (this.actionsPlayerHandler == null
				|| this.actionsPlayerHandler instanceof VirtualPlayerAI) {
			notifyListener();
		}
		PlayerAction playerActionToReturn = new PlayerAction(
				this.player.getDirection(), this.player.getShooting());
		this.player.act();
		if ((this.actionsPlayerHandler == null || this.actionsPlayerHandler instanceof VirtualPlayerAI)
				&& isGameOver()) {
			notifyListener();
		}
		this.aShootingHappened = false;
		return playerActionToReturn;
	}

	public Boolean didAShootingHappen() {
		return this.aShootingHappened;
	}

	private void notifyListener() {
		for (UpdaterListener updatedListener : this.updaterListeners) {
			updatedListener.update(GameManagerMultiplayer.this);
		}
	}

	public void addUpdaterListener(UpdaterListener updatedListener) {
		this.updaterListeners.add(updatedListener);
	}

}
