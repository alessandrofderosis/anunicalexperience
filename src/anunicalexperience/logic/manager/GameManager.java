package anunicalexperience.logic.manager;

import anunicalexperience.logic.objects.character.Player;
import anunicalexperience.logic.world.World;

public interface GameManager {
	public abstract void start();

	public abstract Boolean isGameOver();

	public abstract PlayerAction update();

	public abstract Player getPlayer();

	public abstract World getWorld();

	public abstract void drawMatrix();
}
