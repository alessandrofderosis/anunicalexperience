package anunicalexperience.logic.manager;

import anunicalexperience.logic.objects.Direction;

public class PlayerAction {

	private Direction playerDirection = Direction.NULL;
	private Boolean shoot = false;

	public PlayerAction() {
	}

	public PlayerAction(Direction playerDirection, Boolean shoot) {
		this.playerDirection = playerDirection;
		this.shoot = shoot;
	}

	public Direction getDirection() {
		return this.playerDirection;
	}

	public Boolean getShoot() {
		return this.shoot;
	}

	public void setPlayerDirection(Direction direction) {
		this.playerDirection = direction;
	}

	public void setShoot(boolean shoot) {
		this.shoot = shoot;
	}

}
