package anunicalexperience.logic.manager;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import anunicalexperience.logic.objects.Empty;
import anunicalexperience.logic.objects.LoaderWeapons;
import anunicalexperience.logic.objects.ObjectWithPosition;
import anunicalexperience.logic.objects.Potion;
import anunicalexperience.logic.objects.Vehicle;
import anunicalexperience.logic.objects.character.Player;
import anunicalexperience.logic.objects.character.Professor;
import anunicalexperience.logic.objects.character.Student;
import anunicalexperience.logic.world.WorldImpl;

public class GameManagerSinglePlayer extends AbstractGameManager {

	private ObjectWithPosition[][] getInitialMatrix() throws IOException {
		final ObjectWithPosition[][] defaultWorld = new ObjectWithPosition[WorldImpl.HEIGHT][WorldImpl.WIDTH];
		final BufferedReader bufferedReader = new BufferedReader(
				new FileReader("initialWorld.txt"));
		String buffer;
		int row = 0;
		try {
			while ((buffer = bufferedReader.readLine()) != null) {
				String[] split = buffer.split(";");
				for (int col = 0; col < split.length; col++) {
					defaultWorld[row][col] = getObject(
							Integer.parseInt(split[col]), row, col);
				}
				row++;
			}
		} finally {
			bufferedReader.close();
		}
		return defaultWorld;
	}

	private ObjectWithPosition getObject(final int parseInt, final int row,
			final int col) {
		switch (parseInt) {
		case 0:
			return new Empty(col, row);
		case 1:
			return new Student(col, row);
		case 2:
			return new Professor(col, row);
		case 3:
			return new Vehicle(col, row);
		case 4:
			return new Potion(col, row);
		case 5:
			return new LoaderWeapons(col, row);
		default:
			throw new RuntimeException("type not supported: " + parseInt
					+ " in position: " + row + ":" + col);
		}
	}

	/**
	 * @throws IOException
	 */
	@Override
	public void start() {
		try {
			this.world = new WorldImpl(getInitialMatrix());
		} catch (IOException e) {
			this.player = null;
			e.printStackTrace();
		}
		this.player = new Player(this.world, WorldImpl.WIDTH / 2,
				WorldImpl.HEIGHT - 1);
	}
}
