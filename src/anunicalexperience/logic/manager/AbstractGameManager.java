package anunicalexperience.logic.manager;

import anunicalexperience.logic.objects.Empty;
import anunicalexperience.logic.objects.LoaderWeapons;
import anunicalexperience.logic.objects.ObjectWithPosition;
import anunicalexperience.logic.objects.Obstacle;
import anunicalexperience.logic.objects.Potion;
import anunicalexperience.logic.objects.Vehicle;
import anunicalexperience.logic.objects.character.Player;
import anunicalexperience.logic.objects.character.Professor;
import anunicalexperience.logic.objects.character.Student;
import anunicalexperience.logic.world.World;
import anunicalexperience.logic.world.WorldImpl;

public abstract class AbstractGameManager implements GameManager {
	protected Player player;
	/**
	 * @uml.property name="world"
	 */
	protected World world;

	@Override
	public abstract void start();

	@Override
	public Boolean isGameOver() {
		return this.player.isDead();
	}

	/**
	 * Getter of the property <tt>world</tt>
	 * 
	 * @return Returns the world.
	 * @uml.property name="world"
	 */
	@Override
	public World getWorld() {
		return this.world;
	}

	@Override
	public PlayerAction update() {
		PlayerAction playerActionToReturn = new PlayerAction(
				this.player.getDirection(), this.player.getShooting());
		this.world.update();
		this.player.act();
		return playerActionToReturn;
	}

	/**
	 * Getter of the property <tt>player</tt>
	 * 
	 * @return Returns the player.
	 * @uml.property name="player"
	 */
	@Override
	public Player getPlayer() {
		return this.player;
	}

	/**
	 * Setter of the property <tt>player</tt>
	 * 
	 * @param player
	 *            The player to set.
	 * @uml.property name="player"
	 */
	public void setPlayer(Player player) {
		this.player = player;
	}

	public int getNumberOfObjectWithPosition(
			ObjectWithPosition objectWithPosition) {
		if (objectWithPosition instanceof Empty) {
			return 0;
		}
		if (objectWithPosition instanceof Student) {
			return 1;
		}
		if (objectWithPosition instanceof Professor) {
			return 2;
		}
		if (objectWithPosition instanceof Vehicle) {
			return 3;
		}
		if (objectWithPosition instanceof Potion) {
			return 4;
		}

		if (objectWithPosition instanceof LoaderWeapons) {
			return 5;
		}
		return 99999;
	}

	@Override
	public void drawMatrix() {
		for (int i = 0; i < WorldImpl.HEIGHT; i++) {
			for (int j = 0; j < WorldImpl.WIDTH; j++) {
				ObjectWithPosition cella = this.world.getCell(j, i);
				int cell = getNumberOfObjectWithPosition(cella);
				if (cella instanceof Obstacle && ((Obstacle) cella).isDead()) {
					cell = 0;
				}
				if (i == this.player.getY() && j == this.player.getX()) {
					cell = 9;
				}
				System.out.print(cell + "   ");
			}
			System.out.println();
		}
	}
}
