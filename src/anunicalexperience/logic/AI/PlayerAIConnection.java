package anunicalexperience.logic.AI;

import java.net.Socket;

import anunicalexperience.logic.manager.GameManagerMultiplayer;
import anunicalexperience.logic.manager.GameManagerMultiplayer.UpdaterListener;
import anunicalexperience.logic.network.ConnectionManager;
import anunicalexperience.logic.objects.Direction;

public class PlayerAIConnection implements Runnable {

	@Override
	public void run() {
		while (!this.gameManager.isGameOver()) {
			try {
				Thread.sleep(100);
				this.gameManager.update();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	protected ConnectionManager connectionManager;
	private final GameManagerMultiplayer gameManager;

	public PlayerAIConnection(String ip, int port,
			DifficultyConstant difficultyConstant) throws Exception {
		this.connectToServer(ip, port, "artificialIntelligence");
		VirtualPlayerAI artificialPlayer = new VirtualPlayerAI(
				difficultyConstant);
		this.gameManager = new GameManagerMultiplayer(this.connectionManager,
				artificialPlayer);
		artificialPlayer.setGameManager(this.gameManager);
		this.gameManager.addUpdaterListener(new UpdaterListener() {

			@Override
			public void update(GameManagerMultiplayer gameManager) {
				if (gameManager.isGameOver()) {
					PlayerAIConnection.this.connectionManager.dispatch("LOSE;");
				} else {
					Boolean shoot = gameManager.didAShootingHappen();
					String shootString = "";
					if (shoot) {
						shootString = "s";
					}
					String playerActions = new String(
							directionToString(gameManager.getPlayer()
									.getDirection()) + ";" + shootString + ";");
					PlayerAIConnection.this.connectionManager
							.dispatch(playerActions);
				}
			}

			private String directionToString(Direction direction) {
				switch (direction) {
				case LEFT:
					return "l";
				case RIGHT:
					return "r";
				default:
					return "";
				}
			}
		});
		this.gameManager.start();
		new Thread(this).start();
	}

	private void connectToServer(String ip, int port, String name)
			throws Exception {
		final Socket socket = new Socket(ip, port);
		this.connectionManager = new ConnectionManager(socket, name);
		new Thread(this.connectionManager, "Connection Manager").start();
	}

	public GameManagerMultiplayer getGameManager() {
		return this.gameManager;
	}

}
