package anunicalexperience.logic.AI;

public enum DifficultyConstant {

	RANDOM("EASY"), QUITE_STUPID("MEDIUM"), QUITE_CLEVER("HARD");
	private final String label;

	private DifficultyConstant(String label) {
		this.label = label;
	}

	public String getLabel() {
		return this.label;
	}

	public static DifficultyConstant getDifficultyConstantFromLabel(String label) {
		if (label.equals(DifficultyConstant.RANDOM.getLabel())) {
			return RANDOM;
		}
		if (label.equals(DifficultyConstant.QUITE_STUPID.getLabel())) {
			return QUITE_STUPID;
		}
		if (label.equals(DifficultyConstant.QUITE_CLEVER.getLabel())) {
			return QUITE_CLEVER;
		}
		return RANDOM;
	}
}
