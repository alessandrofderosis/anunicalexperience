package anunicalexperience.logic.AI;

public class DifficultyLevel {

	private final int RANDOM_MODE_SUCCESS_RATE = 2;
	private final int QUITE_STUPID_MODE_SUCCESS_RATE = 4;
	private final int QUITE_CLEVER_MODE_SUCCESS_RATE = 7;

	private int successRate;

	public DifficultyLevel(DifficultyConstant difficultyConstant) {
		if (difficultyConstant == DifficultyConstant.QUITE_CLEVER) {
			this.successRate = this.QUITE_CLEVER_MODE_SUCCESS_RATE;
		} else if (difficultyConstant == DifficultyConstant.QUITE_STUPID) {
			this.successRate = this.QUITE_STUPID_MODE_SUCCESS_RATE;
		} else {
			this.successRate = this.RANDOM_MODE_SUCCESS_RATE;
		}
	}

	public int getSuccessRate() {
		return this.successRate;
	}
}
