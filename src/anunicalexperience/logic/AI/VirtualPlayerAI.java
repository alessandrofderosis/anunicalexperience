package anunicalexperience.logic.AI;

import java.util.ArrayList;

import anunicalexperience.logic.manager.GameManager;
import anunicalexperience.logic.manager.PlayerAction;
import anunicalexperience.logic.network.PlayerActionsHandler;
import anunicalexperience.logic.objects.Book;
import anunicalexperience.logic.objects.Direction;
import anunicalexperience.logic.objects.ObjectWithPosition;
import anunicalexperience.logic.objects.Obstacle;
import anunicalexperience.logic.objects.Potion;
import anunicalexperience.logic.objects.character.Player;
import anunicalexperience.logic.world.World;

public class VirtualPlayerAI implements PlayerActionsHandler {

	private GameManager gameManager = null;
	private final DifficultyConstant difficultyConstant;
	private final DifficultyLevel difficultyLevel;
	private int rangeOfLife;
	private final int DEATH_LINE = 60;
	private PlayerAction playerAction;

	public VirtualPlayerAI(DifficultyConstant difficultyConstant) {
		this.playerAction = new PlayerAction();
		this.difficultyConstant = difficultyConstant;
		this.difficultyLevel = new DifficultyLevel(this.difficultyConstant);
		this.rangeOfLife = 0;
	}

	@Override
	public PlayerAction getAction() {
		this.playerAction = new PlayerAction();
		int randomNumber = (int) (Math.random() * 10);

		if (this.rangeOfLife < this.DEATH_LINE
				|| randomNumber <= this.difficultyLevel.getSuccessRate()) {
			findRoad();
		} else {
			matchRandomDirection();
		}
		this.rangeOfLife++;
		return this.playerAction;
	}

	private Direction matchRandomDirection() {
		int randomDirection = (int) (Math.random() * 3);
		if (randomDirection == 2) {
			return Direction.RIGHT;
		} else if (randomDirection == 0) {
			return Direction.LEFT;
		} else {
			return Direction.NULL;
		}
	}

	private Direction findRoad() {

		Player player = this.gameManager.getPlayer();

		ArrayList<Integer> freeCell = foundFreeColumns(player.getY(),
				player.getX());

		return chooseWay(freeCell);
	}

	private ArrayList<Integer> foundFreeColumns(int y, int x) {

		ArrayList<Integer> columnsToDiscover = new ArrayList<Integer>();
		World secondWorld = this.gameManager.getWorld();

		// if (!(secondWorld.getCell(x, y) instanceof Obstacle)) {
		columnsToDiscover.add(x);
		// }
		ObjectWithPosition cell = secondWorld.getCell(
				secondWorld.getWidth() / 2, y);
		// se il player non si trova al centro e la cella centrale � libera
		if (x != secondWorld.getWidth() / 2
				&& !(secondWorld.getCell(secondWorld.getWidth() / 2, y) instanceof Obstacle)
				|| cell instanceof Obstacle && ((Obstacle) cell).isDead()) {
			columnsToDiscover.add(secondWorld.getWidth() / 2);
		} else if (x == secondWorld.getWidth() / 2) {
			for (int i = 0; i < 3; i++) {
				if (i != secondWorld.getWidth() / 2) {
					ObjectWithPosition currentCell = secondWorld.getCell(i, y);
					if (!(currentCell instanceof Obstacle)
							|| currentCell instanceof Obstacle
							&& ((Obstacle) currentCell).isDead()) {
						columnsToDiscover.add(i);
					}
				}
			}
		}
		return columnsToDiscover;
	}

	private Direction chooseWay(ArrayList<Integer> freeColumns) {

		Player player = this.gameManager.getPlayer();
		int playerPositionX = player.getX();

		/*
		 * se tutte le celle visitabili della riga del player sono occupate da
		 * un ostacolo oppure se il player � invisibile => vai sempre dritto
		 */

		if (freeColumns.size() == 0 || player.isInvisible()
				&& player.getLengthOfInvisibility() < Potion.TIME_LENGTH - 2) {
			this.playerAction.setPlayerDirection(Direction.NULL);
			return Direction.NULL;
		}

		/*
		 * se c'� qualche cella adiacente a quella del player in cui non ci sono
		 * ostacoli => controlliamo se essa ammette una via d'uscita
		 */
		for (int i = 0; i < freeColumns.size(); i++) {

			if (isAFreeWay(freeColumns.get(i))) {

				if (freeColumns.get(i) < playerPositionX) {
					this.playerAction.setPlayerDirection(Direction.LEFT);
					return Direction.LEFT;

				} else if (freeColumns.get(i) > playerPositionX) {
					this.playerAction.setPlayerDirection(Direction.RIGHT);
					return Direction.RIGHT;

				} else {
					this.playerAction.setPlayerDirection(Direction.NULL);
					return Direction.NULL;
				}
			}
		}
		/*
		 * se nessuna cella adiacente ammette una via d'uscita proviamo a
		 * crearci un varco sparando
		 */
		// System.out.println("devo sparare");
		for (int i = 0; i < Book.RANGE_ACTION; i++) {
			ObjectWithPosition currentCell = this.gameManager.getWorld()
					.getCell(player.getX(), player.getY() - i);
			if (currentCell instanceof Obstacle
					&& !((Obstacle) currentCell).isDead()
					&& player.getNumberOfBooks() > 0) {
				this.playerAction.setShoot(true);
				this.playerAction.setPlayerDirection(Direction.NULL);
				return Direction.NULL;
			}
		}
		/*
		 * se non abbiamo pi� libri (non possiamo sparare)=> andiamo dritti in
		 * contro alla morte
		 */
		this.playerAction.setPlayerDirection(Direction.NULL);
		return Direction.NULL;
	}

	/*
	 * analizza le celle della riga del player a esso adiacenti che non sono
	 * occupate da un ostacolo. Per ciascuna di esse tale metodo controlla se la
	 * corsia individuata dal relativo indice ammetta una via d'uscita
	 */

	private boolean isAFreeWay(int freeCell) {
		ArrayList<Integer> freePositions = new ArrayList<Integer>();
		World world = this.gameManager.getWorld();

		for (int row = this.gameManager.getWorld().getHeight() - 2; row >= 0; row--) {

			ObjectWithPosition currentCell = world.getCell(freeCell, row);
			ObjectWithPosition successiveCell = world
					.getCell(freeCell, row - 1);
			if (currentCell instanceof Obstacle
					&& !((Obstacle) currentCell).isDead()
					|| successiveCell instanceof Obstacle
					&& !((Obstacle) successiveCell).isDead()) {
				return false;
			} else {
				/*
				 * se lungo questa corsia c'� almeno una via d'uscita => essa �
				 * sicura
				 */
				freePositions = foundFreeColumns(row, freeCell);
				if (freePositions.size() >= 2) {
					return true;
				}
			}
		}
		/*
		 * se arrivo alla riga 0 del mondo => la via � sicura
		 */
		return true;
	}

	public void setGameManager(GameManager gameManager) {
		this.gameManager = gameManager;
	}

	public DifficultyConstant getDifficultyLevel() {
		return this.difficultyConstant;
	}

	public int getRangeOfLife() {
		return this.rangeOfLife;
	}

	public void setRangeOfLife(int rangeOfLife) {
		this.rangeOfLife = rangeOfLife;
	}
}
