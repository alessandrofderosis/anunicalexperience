package anunicalexperience.logic.network;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;

import anunicalexperience.logic.objects.Empty;
import anunicalexperience.logic.objects.LoaderWeapons;
import anunicalexperience.logic.objects.ObjectWithPosition;
import anunicalexperience.logic.objects.Potion;
import anunicalexperience.logic.objects.Vehicle;
import anunicalexperience.logic.objects.character.Professor;
import anunicalexperience.logic.objects.character.Student;
import anunicalexperience.logic.world.WorldImpl;

public class Server {

	private class ClientManager implements Runnable {

		private String name;

		private PrintWriter pw = null;

		private final ServerGameManager server;

		private final Socket socket;

		private boolean connectedClient;

		public ClientManager(final Socket socket, final ServerGameManager server) {
			this.socket = socket;
			this.server = server;
			try {
				this.pw = new PrintWriter(this.socket.getOutputStream(), true);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		public void dispatch(final String message) {
			this.pw.println(message);
		}

		public String getName() {
			return this.name;

		}

		public boolean isConnected() {
			return this.connectedClient;
		}

		@Override
		public void run() {
			try {
				/*
				 * Il bufferedreader legge quello che il socket stesso scrive
				 * attraverso il printwriter.
				 */
				final BufferedReader br = new BufferedReader(
						new InputStreamReader(this.socket.getInputStream()));
				this.name = br.readLine();
				this.server.dispatchMessage(
						this.server.getConnectedClientNames(), null);
				this.server.setReady(this);
				this.connectedClient = true;
				while (this.connectedClient) {
					final String buffer = br.readLine();
					if (buffer == null) {
						throw new IOException();
					}
					String[] split = buffer.split(";");
					if (split.length > 0 && split[0].equals("LOSE")) {
						this.server.dispatchMessage(buffer, null);
					} else {
						this.server.dispatchMessage(buffer, this);
					}
				}
			} catch (final IOException e) {
				System.out.println("Client disconnected: " + this.name);
				this.connectedClient = false;
			}
		}

	}

	private class ServerGameManager implements Runnable {
		private final Set<ClientManager> clients = new HashSet<Server.ClientManager>();
		private final Set<ClientManager> readyClients = new HashSet<Server.ClientManager>();
		private WorldImpl worldImpl = null;

		public ServerGameManager() {
		}

		public void add(final ClientManager cm) {
			this.clients.add(cm);
		}

		public void dispatchMessage(final String message,
				final ClientManager senderClientManager) {
			for (final ClientManager cm : this.clients) {
				if (cm != senderClientManager) {
					cm.dispatch(message);
				}

			}
		}

		public String getConnectedClientNames() {
			final StringBuilder sb = new StringBuilder();
			for (final ClientManager cm : this.clients) {
				if (cm.getName() != null) {
					sb.append(cm.getName());
					sb.append(";");
				}
			}
			return sb.toString();
		}

		public void setReady(final ClientManager clientManager)
				throws IOException {
			this.readyClients.add(clientManager);
			if (this.readyClients.size() == 2) {
				dispatchMessage("#INITMATRIX", null);
				this.worldImpl = new WorldImpl(this.getInitialMatrix());
				new Thread(this).start();
			}
		}

		private ObjectWithPosition[][] getInitialMatrix() throws IOException {
			final ObjectWithPosition[][] defaultWorld = new ObjectWithPosition[WorldImpl.HEIGHT][WorldImpl.WIDTH];
			final BufferedReader bufferedReader = new BufferedReader(
					new FileReader("initialWorld.txt"));
			String buffer;
			int row = 0;
			try {
				while ((buffer = bufferedReader.readLine()) != null) {
					String[] split = buffer.split(";");
					for (int col = 0; col < split.length; col++) {
						defaultWorld[row][col] = getObject(
								Integer.parseInt(split[col]), row, col);
					}
					row++;
					this.dispatchMessage(buffer, null);
				}
			} finally {
				bufferedReader.close();
			}
			dispatchMessage("#START", null);
			return defaultWorld;
		}

		private String getRowString(ObjectWithPosition[] row) {
			StringBuilder stringRow = new StringBuilder();
			for (ObjectWithPosition objectWithPosition : row) {
				if (objectWithPosition instanceof Empty) {
					stringRow.append("0;");
				} else if (objectWithPosition instanceof Student) {
					stringRow.append("1;");
				} else if (objectWithPosition instanceof Professor) {
					stringRow.append("2;");
				} else if (objectWithPosition instanceof Vehicle) {
					stringRow.append("3;");
				} else if (objectWithPosition instanceof Potion) {
					stringRow.append("4;");
				} else if (objectWithPosition instanceof LoaderWeapons) {
					stringRow.append("5;");
				} else {
					throw new RuntimeException("type not supported");
				}
			}
			return stringRow.toString();
		}

		private ObjectWithPosition getObject(final int parseInt, final int row,
				final int col) {
			switch (parseInt) {
			case 0:
				return new Empty(col, row);
			case 1:
				return new Student(col, row);
			case 2:
				return new Professor(col, row);
			case 3:
				return new Vehicle(col, row);
			case 4:
				return new Potion(col, row);
			case 5:
				return new LoaderWeapons(col, row);
			default:
				throw new RuntimeException("type not supported: " + parseInt
						+ " in position: " + row + ":" + col);
			}
		}

		public void startGame() {

			for (final ClientManager cm : this.clients) {
				new Thread(cm, cm.toString()).start();
			}

		}

		@Override
		public void run() {
			boolean running = true;
			while (running) {
				this.worldImpl.update();
				for (ClientManager client : this.clients) {
					if (client.isConnected()) {
						this.dispatchMessage(
								getRowString(this.worldImpl.getWorldMatrix()[0]),
								null);
					} else {
						running = false;
					}
				}
				try {
					Thread.sleep(350);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

		}
	}

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(final String[] args) throws IOException {
		final Server server = new Server(Integer.parseInt(args[0]));
		server.runServer();
	}

	private final int port;

	private final boolean running = true;

	private ServerSocket serverSocket;

	public Server(final int port) throws IOException {
		this.port = port;
	}

	private void runServer() throws IOException {
		this.serverSocket = new ServerSocket(this.port);
		while (this.running) {
			final ServerGameManager gameManager = new ServerGameManager();
			final Socket socket1 = this.serverSocket.accept();
			final ClientManager cm1 = new ClientManager(socket1, gameManager);
			gameManager.add(cm1);
			final Socket socket2 = this.serverSocket.accept();
			final ClientManager cm2 = new ClientManager(socket2, gameManager);
			gameManager.add(cm2);
			gameManager.startGame();
		}
	}

}
