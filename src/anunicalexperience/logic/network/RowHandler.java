package anunicalexperience.logic.network;

import anunicalexperience.logic.objects.ObjectWithPosition;

public interface RowHandler {
	ObjectWithPosition[] getNewRow(int numberOfUpdate);
}
