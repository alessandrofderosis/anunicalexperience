package anunicalexperience.logic.network;

public class NoALooserException extends Exception {

	public NoALooserException() {
		super("players are playing yet");
	}

}
