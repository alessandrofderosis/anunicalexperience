package anunicalexperience.logic.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import anunicalexperience.logic.manager.PlayerAction;
import anunicalexperience.logic.objects.Direction;
import anunicalexperience.logic.objects.Empty;
import anunicalexperience.logic.objects.LoaderWeapons;
import anunicalexperience.logic.objects.ObjectWithPosition;
import anunicalexperience.logic.objects.Potion;
import anunicalexperience.logic.objects.Vehicle;
import anunicalexperience.logic.objects.character.Professor;
import anunicalexperience.logic.objects.character.Student;
import anunicalexperience.logic.world.WorldImpl;

public class ConnectionManager implements Runnable, PlayerActionsHandler,
		RowHandler {

	private final Queue<PlayerAction> playerActions;

	private final List<ObjectWithPosition[]> worldRows;

	private String enemy;

	private final String name;

	private final Lock actionPlayerLock = new ReentrantLock();

	private final Lock rowsLock = new ReentrantLock();

	private final Condition rowsCondition = this.rowsLock.newCondition();

	private final Condition actionPlayerCondition = this.actionPlayerLock
			.newCondition();

	private boolean waitAction = false;

	private PrintWriter pw;

	private final Socket socket;

	private BufferedReader br;

	private String buffer;

	public ConnectionManager(final Socket socket, final String name) {
		this.socket = socket;
		this.name = name;
		this.playerActions = new LinkedList<PlayerAction>();
		this.worldRows = new ArrayList<ObjectWithPosition[]>();
	}

	public void close() {
		try {
			this.socket.close();
		} catch (final IOException e) {
		}
	}

	public void dispatch(final String message) {
		this.pw.println(message);
	}

	public String getEnemyName() {
		return this.enemy;
	}

	public String getPlayerName() {
		return this.name;
	}

	@Override
	public void run() {
		try {
			this.br = new BufferedReader(new InputStreamReader(
					this.socket.getInputStream()));
			this.pw = new PrintWriter(this.socket.getOutputStream(), true);
			this.pw.println(getPlayerName());
			this.buffer = this.br.readLine();
			readEnemyName();
			readInitialMatrix();
			this.buffer = this.br.readLine();
			int numOfloosers = 0;
			while (numOfloosers < 2 || this.waitAction) {
				String[] split = this.buffer.split(";");
				if (split.length > 0 && split[0].equals("LOSE")) {
					numOfloosers++;
				} else if (split.length > 0 && split[0].length() > 0
						&& Character.isDigit(split[0].toCharArray()[0])) {
					readRow(split);
				} else {
					readAction(split);
				}
				this.buffer = this.br.readLine();
			}
		} catch (final IOException e) {
			System.out.println("Connection closed");
		}
		close();
	}

	private void readRow(final String[] split) {
		ObjectWithPosition[] rows = new ObjectWithPosition[WorldImpl.WIDTH];
		for (int i = 0; i < split.length; i++) {
			rows[i] = getObject(Integer.parseInt(split[i]), 0, i);
		}

		this.rowsLock.lock();
		this.worldRows.add(rows);
		this.rowsCondition.signalAll();
		this.rowsLock.unlock();
	}

	private void readAction(final String[] split) {
		Direction playerDirection = Direction.NULL;

		if (split.length == 0 || split[0].length() == 0) {
			playerDirection = Direction.NULL;
		} else if (split[0].equals("r")) {
			playerDirection = Direction.RIGHT;
		} else if (split[0].equals("l")) {
			playerDirection = Direction.LEFT;
		}
		this.actionPlayerLock.lock();
		if (split.length >= 2 && split[1].length() > 0 && split[1].equals("s")) {
			this.playerActions.add(new PlayerAction(playerDirection, true));
		} else {
			this.playerActions.add(new PlayerAction(playerDirection, false));
		}
		this.actionPlayerCondition.signalAll();
		this.actionPlayerLock.unlock();
	}

	private void readInitialMatrix() throws IOException {
		int numberOfRow = 0;
		while (!this.buffer.equals("#START")) {
			final String[] split = this.buffer.split(";");
			ObjectWithPosition[] objectWithPositions = new ObjectWithPosition[WorldImpl.WIDTH];
			if (split.length != 0) {
				for (int i = 0; i < split.length; i++) {
					objectWithPositions[i] = getObject(
							Integer.parseInt(split[i]), numberOfRow, i);
				}
			}
			this.rowsLock.lock();
			this.worldRows.add(objectWithPositions);
			this.rowsCondition.signalAll();
			this.rowsLock.unlock();
			this.buffer = this.br.readLine();
			numberOfRow++;
		}
	}

	private void readEnemyName() throws IOException {
		while (!this.buffer.equals("#INITMATRIX")) {
			final String[] split = this.buffer.split(";");
			if (split.length != 0) {
				for (final String name : split) {
					if (!name.equals(getPlayerName())) {
						this.enemy = name;
					}
				}
			}
			this.buffer = this.br.readLine();
		}
		this.buffer = this.br.readLine();
	}

	private ObjectWithPosition getObject(final int objectIndex, final int row,
			final int col) {
		switch (objectIndex) {
		case 0:
			return new Empty(col, row);
		case 1:
			return new Student(col, row);
		case 2:
			return new Professor(col, row);
		case 3:
			return new Vehicle(col, row);
		case 4:
			return new Potion(col, row);
		case 5:
			return new LoaderWeapons(col, row);
		default:
			throw new RuntimeException("type not supported: " + objectIndex
					+ " in position: " + row + ":" + col);
		}
	}

	@Override
	public PlayerAction getAction() {
		this.actionPlayerLock.lock();
		try {
			while (this.playerActions.isEmpty()) {
				try {
					this.waitAction = true;
					this.actionPlayerCondition.await();
				} catch (InterruptedException e) {
					throw new RuntimeException(
							" Problems with Action condition");
				}
			}
			this.waitAction = false;
			return this.playerActions.remove();
		} finally {
			this.actionPlayerLock.unlock();
		}
	}

	@Override
	public ObjectWithPosition[] getNewRow(int numberOfUpdate) {
		if (numberOfUpdate <= 0) {
			throw new RuntimeException("Number of update must to be positive");
		}
		this.rowsLock.lock();
		try {
			while (numberOfUpdate > this.worldRows.size()) {
				try {
					this.rowsCondition.await();
				} catch (InterruptedException e) {
					throw new RuntimeException(" Problems with row condition");
				}
			}
			if (this.worldRows.get(numberOfUpdate - 1) == null) {
				System.out.println("row is null");
			}
			return this.worldRows.get(numberOfUpdate - 1);
		} finally {
			this.rowsLock.unlock();
		}
	}
}
