package anunicalexperience.logic.network;

import java.net.Socket;

import anunicalexperience.logic.manager.GameManagerMultiplayer;
import anunicalexperience.logic.manager.GameManagerMultiplayer.UpdaterListener;
import anunicalexperience.logic.objects.Direction;

public class MultiPlayer {

	protected ConnectionManager connectionManager;
	private final GameManagerMultiplayer myGameManager;
	private final GameManagerMultiplayer enemyGameManager;

	public MultiPlayer(String ip, int port, String name) throws Exception {
		this.connectToServer(ip, port, name);
		this.myGameManager = new GameManagerMultiplayer(this.connectionManager,
				null);

		this.enemyGameManager = new GameManagerMultiplayer(
				this.connectionManager, this.connectionManager);
		this.myGameManager.addUpdaterListener(new UpdaterListener() {

			@Override
			public void update(GameManagerMultiplayer gameManager) {
				if (gameManager.isGameOver()) {
					MultiPlayer.this.connectionManager.dispatch("LOSE;");
				} else {
					Boolean shoot = gameManager.didAShootingHappen();
					String shootString = "";
					if (shoot) {
						shootString = "s";
					}
					String playerActions = new String(
							directionToString(gameManager.getPlayer()
									.getDirection()) + ";" + shootString + ";");
					MultiPlayer.this.connectionManager.dispatch(playerActions);
				}
			}

			private String directionToString(Direction direction) {
				switch (direction) {
				case LEFT:
					return "l";
				case RIGHT:
					return "r";
				default:
					return "";
				}
			}
		});
	}

	private void connectToServer(String ip, int port, String name)
			throws Exception {
		final Socket socket = new Socket(ip, port);
		this.connectionManager = new ConnectionManager(socket, name);
		new Thread(this.connectionManager, "Connection Manager").start();
	}

	public GameManagerMultiplayer getMyGameManager() {
		return this.myGameManager;
	}

	public GameManagerMultiplayer getEnemyGameManager() {
		return this.enemyGameManager;
	}

	public String getLooser() throws NoALooserException {
		if (this.myGameManager.isGameOver()
				&& this.enemyGameManager.isGameOver()) {
			if (this.myGameManager.getPlayer().getDistance() > this.enemyGameManager
					.getPlayer().getDistance()) {
				return this.connectionManager.getEnemyName();
			} else if (this.myGameManager.getPlayer().getDistance() < this.enemyGameManager
					.getPlayer().getDistance()) {
				return this.connectionManager.getPlayerName();
			} else {
				return "draw";
			}
		} else {
			throw new NoALooserException();
		}
	}

}
