package anunicalexperience.logic.network;

import anunicalexperience.logic.manager.PlayerAction;


public interface PlayerActionsHandler {
	PlayerAction getAction();
}
