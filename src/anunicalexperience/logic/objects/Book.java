package anunicalexperience.logic.objects;

import anunicalexperience.logic.world.World;

/**
 * @uml.dependency supplier="Logic.Weapon"
 */
public class Book extends AbstractMoveableUpDown implements Weapon {

	/**
	 * @uml.property name="rangeAction" readOnly="true"
	 */
	public static final int RANGE_ACTION = 8;
	private final int POINTS = 100;
	private final World world;

	public Book(int x, int y, World world) {
		super(x, y);
		this.world = world;
	}

	/**
	 * Getter of the property <tt>rangeAction</tt>
	 * 
	 * @return Returns the rangeAction.
	 * @uml.property name="rangeAction"
	 */
	@Override
	public int getRangeAction() {
		return Book.RANGE_ACTION;
	}

	/**
	 * This method provides the current position of the book shot; It returns
	 * false if it hits a professor. It returns TRUE otherwise;
	 */
	public Obstacle update() {
		for (int i = 0; i < Book.RANGE_ACTION; i++) {
			ObjectWithPosition cell = this.world.getCell(this.getX(), this.y);
			if (cell instanceof Obstacle && !((Obstacle) cell).isDead()) {
				((Obstacle) cell).setAlive(false);
				return (Obstacle) cell;
			}
			this.y--;
		}
		return null;
	}

	@Override
	public int getPoints() {
		return this.POINTS;
	}

}
