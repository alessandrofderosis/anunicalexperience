package anunicalexperience.logic.objects;

import anunicalexperience.logic.objects.character.Bonus;

public class LoaderWeapons extends AbstractMoveableUpDown implements Bonus {

	private final int WEAPONS = 5;
	public static final int CHECKPOINT = 143;
	public static final int POINTS = 150;

	public LoaderWeapons(int x, int y) {
		super(x, y);
	}

	public int getWeapons() {
		return this.WEAPONS;
	}

	@Override
	public int getPoints() {
		return LoaderWeapons.POINTS;
	}

}
