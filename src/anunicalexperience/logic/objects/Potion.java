package anunicalexperience.logic.objects;

import anunicalexperience.logic.objects.character.Bonus;

public class Potion extends AbstractMoveableUpDown implements Bonus {

	public static final int CHECKPOINT = 152;
	private final int POINTS;
	// number of updates while invisibility is available
	public static final int TIME_LENGTH = 30;

	public Potion(int x, int y) {
		super(x, y);
		this.POINTS = 100;
	}

	@Override
	public int getPoints() {
		return this.POINTS;
	}

}
