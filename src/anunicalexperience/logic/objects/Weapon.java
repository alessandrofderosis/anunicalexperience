package anunicalexperience.logic.objects;

/**
 * @uml.dependency supplier="Logic.ObjectWithPosition"
 */
public interface Weapon extends ObjectWithPosition, HasPoints {

	public abstract int getRangeAction();

}
