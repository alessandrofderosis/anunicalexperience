package anunicalexperience.logic.objects;

public abstract class AbstractObjectWithPosition implements ObjectWithPosition {

	protected int x;
	protected int y;

	public AbstractObjectWithPosition(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}

	@Override
	public int getX() {
		return this.x;
	}

	@Override
	public int getY() {
		return this.y;
	}

}
