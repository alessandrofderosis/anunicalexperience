package anunicalexperience.logic.objects;

public interface HasPoints {

	public abstract int getPoints();

}
