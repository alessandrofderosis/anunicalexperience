package anunicalexperience.logic.objects;

public class Vehicle extends AbstractMoveableUpDown implements Obstacle {

	private Boolean alive;
	private final int POINTS = 150;

	public Vehicle(int x, int y) {
		super(x, y);
		this.alive = true;
	}

	@Override
	public void setAlive(Boolean alive) {
		this.alive = false;
	}

	@Override
	public Boolean isDead() {
		return !this.alive;
	}

	@Override
	public int getPoints() {
		return this.POINTS;
	}

}
