package anunicalexperience.logic.objects;

public interface MoveableUpDown extends ObjectWithPosition {

	public abstract void setY(int y);

}
