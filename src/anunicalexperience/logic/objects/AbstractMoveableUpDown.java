package anunicalexperience.logic.objects;

public abstract class AbstractMoveableUpDown extends AbstractObjectWithPosition
		implements MoveableUpDown {

	public AbstractMoveableUpDown(int x, int y) {
		super(x, y);
	}

	@Override
	public void setY(int y) {
		this.y = y;
	}
}
