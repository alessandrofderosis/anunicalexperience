package anunicalexperience.logic.objects;

public interface ObjectWithPosition {

	public abstract int getX();

	public abstract int getY();

}
