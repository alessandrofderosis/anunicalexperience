package anunicalexperience.logic.objects.character;

public class Professor extends AbstractEnemy {

	private final int POINTS = -50;

	public Professor(int x, int y) {
		super(x, y);
	}

	@Override
	public int getPoints() {
		return this.POINTS;
	}
}
