package anunicalexperience.logic.objects.character;

import anunicalexperience.logic.objects.Book;
import anunicalexperience.logic.objects.Direction;
import anunicalexperience.logic.objects.LoaderWeapons;
import anunicalexperience.logic.objects.ObjectWithPosition;
import anunicalexperience.logic.objects.Obstacle;
import anunicalexperience.logic.objects.Potion;
import anunicalexperience.logic.world.World;

public class Player extends AbstractCharacter {

	private long distance;

	/**
	 * @uml.property name="score"
	 */
	private int score;
	/**
	 * /**
	 * 
	 * @uml.property name="numberOfBooks"
	 */
	private int numberOfBooks;

	/**
	 * @uml.property name="direction"
	 */
	private Direction direction = Direction.NULL;

	private Boolean invisible;

	private final World world;

	private int lengthOfInvisibility;

	private boolean shooting;

	private Obstacle lastShotObstacle = null;

	public Player(World world, int x, int y) {
		super(x, y);
		this.score = 0;
		this.numberOfBooks = 10;
		this.invisible = false;
		this.distance = 2;
		this.lengthOfInvisibility = 0;
		this.shooting = false;
		this.world = world;
	}

	public long getDistance() {
		return this.distance;
	}

	public void setDistance(long distance) {
		this.distance = distance;
	}

	public void act() {
		if (this.shooting) {
			this.lastShotObstacle = shootBook();
			this.shooting = false;
		} else {
			if (this.direction == Direction.RIGHT
					&& this.getX() < this.world.getWidth() - 1) {
				this.x = this.getX() + 1;
			} else if (this.direction == Direction.LEFT && this.getX() >= 1) {
				this.x = this.getX() - 1;
			}
			this.direction = Direction.NULL;
		}
		this.update();
	}

	void update() {

		this.distance += 50;

		if (this.invisible) {
			if (this.lengthOfInvisibility >= Potion.TIME_LENGTH) {
				this.invisible = false;
				this.lengthOfInvisibility = 0;
			} else {
				this.lengthOfInvisibility++;
			}
		}

		ObjectWithPosition cell = this.world.getCell(this.getX(), this.getY());
		if (cell instanceof Obstacle && !this.invisible) {
			if (!((Obstacle) cell).isDead()) {
				this.setAlive(false);
			}
		}
		if (cell instanceof LoaderWeapons) {
			LoaderWeapons loaderWeapons = (LoaderWeapons) cell;
			this.numberOfBooks += loaderWeapons.getWeapons();
		}
		if (cell instanceof Potion) {
			this.invisible = true;
			this.lengthOfInvisibility++;
			this.setScore(this.getScore() + ((Potion) cell).getPoints());
		}

	}

	/**
	 * Getter of the property <tt>score</tt>
	 * 
	 * @return Returns the score.
	 * @uml.property name="score"
	 */
	public int getScore() {
		return this.score;
	}

	/**
	 * Setter of the property <tt>score</tt>
	 * 
	 * @param score
	 *            The score to set.
	 * @uml.property name="score"
	 */
	public void setScore(int score) {
		this.score = score;
	}

	/**
	 * Getter of the property <tt>numberOfBooks</tt>
	 * 
	 * @return Returns the numberOfBooks.
	 * @uml.property name="numberOfBooks"
	 */
	public int getNumberOfBooks() {
		return this.numberOfBooks;
	}

	/**
	 * Setter of the property <tt>numberOfBooks</tt>
	 * 
	 * @param numberOfBooks
	 *            The numberOfBooks to set.
	 * @uml.property name="numberOfBooks"
	 */
	public void setNumberOfBooks(int numberOfBooks) {
		this.numberOfBooks = numberOfBooks;
	}

	public void shoot() {
		if (this.numberOfBooks > 0) {
			this.shooting = true;
		}
	}

	public Boolean getShooting() {
		return this.shooting;
	}

	/**
	 * This method throw a book ahead the player. ShootBook set the initial
	 * positions of that book and it can call "update" of book class. Finally it
	 * updates the number of books.
	 */
	private Obstacle shootBook() {
		if (this.numberOfBooks <= 0) {
			return null;
		}
		Book book = new Book(super.getX(), super.getY(), this.world);
		Obstacle obstacle = book.update();
		if (obstacle != null) {
			this.score += obstacle.getPoints();
		}
		this.numberOfBooks--;
		return obstacle;
	}

	/**
	 * Getter of the property <tt>direction</tt>
	 * 
	 * @return Returns the direction.
	 * @uml.property name="direction"
	 */
	public Direction getDirection() {
		return this.direction;
	}

	/**
	 * Setter of the property <tt>direction</tt>
	 * 
	 * @param direction
	 *            The direction to set.
	 * @uml.property name="direction"
	 */
	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	/**
	 * Getter of the property <tt>invisible</tt>
	 * 
	 * @return Returns the invisible.
	 * @uml.property name="invisible"
	 */
	public Boolean isInvisible() {
		return this.invisible;
	}

	/**
	 * Setter of the property <tt>invisible</tt>
	 * 
	 * @param inisible
	 *            The invisible to set.
	 * @uml.property name="invisible"
	 */
	public void setInvisible(Boolean invisible) {
		this.invisible = invisible;
	}

	@Override
	public void reset() {
		this.score = 0;
		this.numberOfBooks = 0;
		this.invisible = false;
	}

	public int getLengthOfInvisibility() {
		return this.lengthOfInvisibility;
	}

	public World getWorld() {
		return this.world;
	}

	public Obstacle getLastShotObstacle() {
		return this.lastShotObstacle;
	}

}
