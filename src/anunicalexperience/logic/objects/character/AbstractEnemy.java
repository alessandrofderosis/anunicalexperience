package anunicalexperience.logic.objects.character;

import anunicalexperience.logic.objects.Obstacle;

public abstract class AbstractEnemy extends AbstractCharacter implements
		Obstacle {

	/**
	 * @uml.property name="points"
	 */
	public AbstractEnemy(int x, int y) {
		super(x, y);
	}

	@Override
	public void reset() {
		this.setAlive(true);
	}

	@Override
	public void setY(int y) {
		this.y = y;
	}
}
