package anunicalexperience.logic.objects.character;

public class Student extends AbstractEnemy {

	private final int POINTS = 50;

	public Student(int x, int y) {
		super(x, y);
	}

	@Override
	public int getPoints() {
		return this.POINTS;
	}

}
