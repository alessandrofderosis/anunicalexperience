package anunicalexperience.logic.objects.character;

import anunicalexperience.logic.objects.AbstractObjectWithPosition;

public abstract class AbstractCharacter extends AbstractObjectWithPosition {

	/**
	 * @uml.property name="alive"
	 */
	private Boolean alive;

	public AbstractCharacter(int x, int y) {
		super(x, y);
		this.alive = true;
	}

	public Boolean isDead() {
		return !this.alive;
	}

	/**
	 * Setter of the property <tt>alive</tt>
	 * 
	 * @param alive
	 *            The alive to set.
	 * @uml.property name="alive"
	 */
	public void setAlive(Boolean alive) {
		this.alive = alive;
	}

	/**
	 */
	public abstract void reset();

}
