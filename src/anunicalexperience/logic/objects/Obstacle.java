package anunicalexperience.logic.objects;

public interface Obstacle extends MoveableUpDown, HasPoints {

	public abstract void setAlive(Boolean alive);

	public abstract Boolean isDead();

}
